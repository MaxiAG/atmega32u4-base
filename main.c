#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>

int main() {
    DDRD |= (1 << DDD5);
    while (1) {
        PORTD |= (1 << PORTD5);
        _delay_ms(500);
        PORTD &= ~(1 << PORTD5);
        _delay_ms(500);
    }
}
